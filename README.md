# Gamer.com

Masters web technologies project; build and test an online store including user registration and login.

## Versions

current version 1.0

### v1.0
As submitted for marking.

### v1.1
SQL injection risk - fix current usage of prepared statements to fit OWASP [guidlines.][1]

[1]: https://www.owasp.org/index.php/SQL_Injection_Prevention_Cheat_Sheet "OWASP SQL injection info"
